export declare const HEADER_TENANT_ID = "x-tenant-id";
export declare const HEADER_ACCESS_TOKEN = "x-access-token";
export declare const HEADER_TIMESTAMP = "x-timestamp";
export declare const HEADER_APP_ID = "nop-app-id";
export declare const HEADER_VERSION = "x-version";
