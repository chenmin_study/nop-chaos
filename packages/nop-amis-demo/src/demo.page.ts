export default  {
    type: "page",
    body: [
        {
            type: "input-text"
        },
        {
            type: "popup-editor",
            popup: {
                type : "input-text"
            }
        }
    ]
}